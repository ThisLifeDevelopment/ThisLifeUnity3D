#Contents
1. [Development Guide](#Development)
2. [External Sources](#External)
3. [Modding](#Modding)

##Development Guide <a name="Development"></a>
This code should use ONLY free assets. Use of non-free assets will complicate licensing.

A new game engine will be needed.

Once LUA becomes available, try to define most assets in LUA scripts. This will make transitioning to the standalone engine easier, as it will be safe to re-implement the API.

##External Sources <a name="External"></a>
[__Unity__ game engine (unity.com)]()

##Modding <a name="Modding"></a>
Interested in Modding? That's fair! So am I!

When I get this functionality working, most of the game elements will be defined in LUA and added to the mod folder by default. This will hopefully provide a useful template.

Considering something Hacky? Please reach out first! If I can give you a better way to do something, that will be good for both of us!

WARNING: The game is currently undergoing rapid development. There may* be breaking changes without warning.

*Will. There Will be breaking changes. I'll do my best to keep it relatively stable, but you will want to tell users what version you wrote for.

I'll set up a "Stable" version when I'm ready for the first major release.
