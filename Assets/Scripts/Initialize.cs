using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Initialize : MonoBehaviour
{
  public int AreaSize;
  public int renderZone;
  private Quaternion rotation;
  //Work around to allow adjusting based on actual area size
  public GameObject area;
  [System.NonSerialized]
    public Areaprops[,] activeArea;
    // Start is called before the first frame update
    void Start()
    {
      Areaprops.size = AreaSize;
      genearateWorld();
    }

    // Update is called once per frame
    void Update()
    {
      //if camera is out of area
      shiftWorld();
    }

    void genearateWorld()
    {
      activeArea = new Areaprops[renderZone,renderZone];
      float shift=AreaSize*renderZone/2;

      for(int i=0; i<renderZone*renderZone; i++)
      {
        float x=(int)i/renderZone,y=i%renderZone;
        activeArea[i/renderZone,i%renderZone] = new Areaprops(new Vector2(x,y),area);
      }
    }
    void shiftWorld()
    {

    }
}
/*
Later box: DO NOT DO THIS NOW - YOU NEED TO WORK ON SIMPLE ACTIONS AND UI FIRST

Move area to dictionary

Load and unload as needed
*/

public class Areaprops
{
    public GameObject area; //The GameObject that represents this space
    public static int size; //All areas should be the same size
    private Quaternion rotation;
    //Integer keys stating the object's location.
    public Vector2 position;
    //Signals that an area is ready to be cleaned up.
    //Check the lock count first - areas can be re-locked at any time.
    UnityEvent m_AllLocksCleared;
    //m_MyEvent.AddListener(MyAction);
    //Add one to this number if a process needs this area to stay loaded.
    //Remove one from this number to signal that it is okay for this area to be cleaned up.
    private int areaLock;
    //Signal that an area is in use and should not be unloaded
    public Areaprops(Vector2 xy_Coords,GameObject areaType)
    {
      m_AllLocksCleared = new UnityEvent();
      position=new Vector2(xy_Coords[0],xy_Coords[1]);

      Vector3 location= new Vector3(xy_Coords[0]*size-(0.5F*size),0,xy_Coords[1]*size-0.5F*size);
      area = GameObject.Instantiate(areaType,location,rotation);
    }
    ~Areaprops()
    {
      if (area != null)
      {
        GameObject.Destroy(area);
      }
    }
    void lockArea()
    {
      areaLock++;
    }
    //Signal that a process using the area has finished.
    void unlockArea()
    {
      areaLock--;
      if( areaLock==0 && m_AllLocksCleared!=null)
      {
        m_AllLocksCleared.Invoke();
      }
    }
}
