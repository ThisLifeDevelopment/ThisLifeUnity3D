using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
  public GameObject currentCamera;
  public float speed;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
      moveCamera();
    }
    void moveCamera()
    {
      float h = Input.GetAxisRaw("Horizontal");
      float v = Input.GetAxisRaw("Vertical");

      currentCamera.transform.position = new Vector3(transform.position.x + (h * speed),transform.position.y, transform.position.z + (v * speed));
    }

}
