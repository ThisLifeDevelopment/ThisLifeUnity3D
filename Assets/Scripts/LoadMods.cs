using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

public class LoadMods : MonoBehaviour
{
    public List<string> modDir = new List<string>();
    public List<ModFolder> mods = new List<ModFolder>();

    private void addDirectory(string dir)
    {
      if (System.IO.Directory.Exists(dir))
      {
         modDir.Add(dir);
      }
    }
    //Locate valid mods - do not interpret mods yet
    private void addMods(string dir)
    {
      string[] Directories = System.IO.Directory.GetDirectories(dir);
      foreach(string directory in Directories)
      {
        //check each directory for init.lua
        Debug.Log($"Found mod in {directory}");
        ModFolder Mod = new ModFolder(directory);
        //if the mod appears valid, add it to the list of available mods
        if(Mod.loadDetails())
        {
          mods.Add(new ModFolder(directory));
        }

      }
    }
    // Start is called before the first frame update
    void Start()
    {
      //Add supported mod folders here
      //Also fix this so that it's system agnostic - the change between \ and / will hurt
      addDirectory($"{System.IO.Directory.GetCurrentDirectory()}/Mods");


      foreach (string dir in modDir)
      {
        addMods(dir);
      }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
public class ModFolder
{
  public string modFolder;
  public Dictionary<string,string> config;
  public List<string> depends;

  public ModFolder(string filepath)
  {
    modFolder=filepath;
    depends = new List<string>();
  }
  ~ModFolder()
  {

  }
  //Process the mod's metadata
  public bool loadDetails()
  {
    //read in name
    readConfig();
    //load in depends
    readDepends();
    return true;
  }
  public void loadMod()
  {

  }
  public void readConfig()
  {
    //foreach(string line in dependstxt)
    //{
    //depends.Add();
    //}
  }
  public void readDepends()
  {
    //foreach(string line in dependstxt)
    //{
    //depends.Add();
    //}
  }
}
